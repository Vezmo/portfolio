ODEUM

10 weeks (6 people)

Roles in this project:

- Creative Direction (Core Gameplay, Spells, Themes, Mechanics, Game Feel, UI, Overall Player Experience)
- Gameplay Programming (StateMachine, Abilities, Movement, Camera, Input Parsing, etc.)
- Animations (Created 100% of all the animations)
- VFXs (Created ~90% of all VFXs)
- Sound Design (Created 100% of all sound effects)
- Level Design (100% of level planning, ~15% of level modeling, ~85% of object placing)
- Composed two small music loops (ambiant music and fight room music)

Responsible for the game's feel : Player Input-> StateMachine -> Animations -> VFXs + Sounds (Feedback)

PRIZE:
Best quality of 3Cs (Camera, Character, Controls) - 2000$

I worked around 50h/week on this project.



